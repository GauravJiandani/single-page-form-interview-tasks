<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Gaurav',
                'employeeId' => '1gaurav',
                'password' => '1Gaurav'
            ],
            [
                'name' => 'Tejas',
                'employeeId' => '2tejas',
                'password' => '1Tejas'
            ],
            [
                'name' => 'Abhinav',
                'employeeId' => '3abhinav',
                'password' => '3Abhinav'
            ]
        ];

        // using collection method to loop over each user and insert it in respective table
        collect($users)->map(function ($user) {
            return User::create([
                'name' => $user['name'],
                'employee_id' => strtolower($user['employeeId']),
                'password' => password_hash($user['password'], PASSWORD_DEFAULT)
            ]);
        });

        echo "Users table seeded" . PHP_EOL;
    }
}
