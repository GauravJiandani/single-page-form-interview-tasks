<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin' => 'Admin',
            'co_admin' => 'Co-Admin',
            'sales_manager' => 'Sales Manager',
            'inventory_exec' => 'Inventory Executive',
            'sale_rep' => 'Sales Representative'
        ];

        foreach ($roles as $roleSlug => $roleName) {
            Role::create([
                'name' => $roleName,
                'slug' => $roleSlug
            ]);
        }

        $tejas = User::where('employee_id', '2tejas')->first();
        $adminRole = Role::where('slug', 'admin')->first();
        $tejas->roles()->attach($adminRole);

        $coAdminRole = Role::where('slug', 'co_admin')->first();
        $abhinav = User::where('employee_id', '3abhinav')->first();
        $abhinav->roles()->attach($coAdminRole);

        $gaurav = User::where('employee_id', '1gaurav')->first();
        $inventoryExec = Role::where('slug', 'inventory_exec')->first();
        $gaurav->roles()->attach($inventoryExec);
        $gaurav->roles()->attach($coAdminRole);

        echo "User role seeded" . PHP_EOL;
    }
}
