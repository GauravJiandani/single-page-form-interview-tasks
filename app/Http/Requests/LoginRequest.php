<?php

namespace App\Http\Requests;

use App\Http\Requests\APIFormRequest;

class LoginRequest extends APIFormRequest
{
    public function __construct()
    {
        parent::__construct();
        $this->transformToLowerCase('employeeId');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employeeId' => 'required|bail|string|min:2|max:255',
            'password' => 'required|bail|string'
        ];
    }
}
