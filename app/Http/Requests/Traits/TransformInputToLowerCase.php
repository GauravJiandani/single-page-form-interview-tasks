<?php

namespace App\Http\Requests\Traits;

trait TransformInputToLowerCase
{
    /**
     * Transform the input with the given key to lowercase.
     *
     * @param string $key
     * @return void
     */
    public function transformToLowerCase(string $key)
    {
        $input = request()->all();
        $value = request()->get($key);
        $input[$key] = strtolower($value);
        request()->replace($input);
    }
}
