<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CheckIfPasswordPerished
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // get user via employeeId
        $employeeId = $request->route('employeeId');
        $user = $this->getUser($employeeId);

        // get password_updated_at field, if null return created_at field
        $passwordUpdatedAt = new Carbon(($user->password_updated_at) ? $user->password_updated_at : $user->created_at);

        // check difference in timestamp, if its more than a month
        if (Carbon::now()->diffInMonths($passwordUpdatedAt) > 1) {
            return redirect()->route('update-password', ['forceUpdatePassword' => true]);
        }

        return $next($request);
    }

    /**
     * Get user from employeeId
     */
    public function getUser($employeeId)
    {
        return User::where('employee_id', $employeeId)->first();
    }
}
