<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    const ROLE_PRIORITY = [
        'admin' => 1,
        'co_admin' => 2,
        'sales_manager' => 2,
        'inventory_exec' => 3,
        'sale_rep' => 3
    ];

    /**
     * Find highest priority role assigned to user
     * @path role/highest-priority/{employeeId}
     * @method GET
     * @param string $employeeId
     * @return Role $role
     */
    public function findHighestPriorityRole(string $employeeId)
    {
        $employeeId = strtolower($employeeId);
        $user = User::with('roles')->where('employee_id', $employeeId)->first();

        if (!$user) {
            return response()->json(['msg' => 'user not found', 404]);
        }

        $initialLowestPriorityRole = 'sale_rep';

        $highestPriorityRole = $user->roles->pluck('slug')->reduce(function ($highestRole, $role) {
            $highestRolePriority = self::ROLE_PRIORITY[$highestRole];
            $currentRolePriority = self::ROLE_PRIORITY[$role];
            // comparing current role priority and highest role priority till now
            // if current role priority is high (means number is low)
            if ($currentRolePriority < $highestRolePriority) {
                $highestRole = $role;
            }
            return $highestRole;
        }, $initialLowestPriorityRole);

        return response()->json(['msg' => 'success', 'highest_priority_role' => $highestPriorityRole], 200);
    }

    /**
     * Assign Role to user
     * @path user/{employeeId}/assign/role/{roleSlug}
     * @param string $employeeId
     * @param string $roleSlug
     * @return json_response
     */
    public function assignRole(string $employeeId, string $roleSlug)
    {
        $employeeId = strtolower($employeeId);
        $user = User::where('employee_id', $employeeId)->first();

        if (!$user) {
            return response()->json(['msg' => 'user not found'], 404);
        }

        $role = Role::where('slug', $roleSlug)->first();

        if (!$role) {
            return response()->json(['msg' => 'role not found'], 404);
        }

        $user->roles()->syncWithoutDetaching($role);

        return response()->json(['msg' => 'success', 'data' => $user->roles], 200);
    }

    /**
     * Remove Role of user
     * @path user/{employeeId}/expel/role/{roleSlug}
     * @param string $employeeId
     * @param string $roleSlug
     * @return json_response
     */
    public function expelRole(string $employeeId, string $roleSlug)
    {
        $employeeId = strtolower($employeeId);
        $user = User::where('employee_id', $employeeId)->first();

        if (!$user) {
            return response()->json(['msg' => 'user not found'], 404);
        }

        $role = Role::where('slug', $roleSlug)->first();

        if (!$role) {
            return response()->json(['msg' => 'role not found'], 404);
        }

        $user->roles()->detach($role);

        return response()->json(['msg' => 'success', 'data' => $user->roles], 200);
    }
}
