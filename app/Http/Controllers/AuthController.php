<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;

/**
 * API Login Controller
 * @package   App\Http\Controllers
 * @author    Gaurav Jiandani <jiandanigaurav@gmail.com>
 */
class AuthController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     * @method GET
     * @return view auth.login
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Show the application's home page.
     * @param mixed $employeeId
     * @method GET
     * @return view home
     */
    public function showHomePage($employeeId)
    {
        $loggedInId = request()->session()->get('loggedInId');
        if (strcmp( strtolower($employeeId), strtolower($loggedInId) ) === 0) {
            return view('home');
        }
        return Redirect::route('login');
    }

    /**
     * Handle a authentication request to the application.
     * @method POST
     * @param  App\Http\Requests\LoginRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(LoginRequest $request)
    {
        $inputEmployeeId = $request->input('employeeId');
        // the password is hashed
        $inputPassword = $request->input('password');

        // retrieving the user
        $user = User::where('employee_id', $inputEmployeeId)->first();

        // user does not exist
        if (!$user) {
            return response()->json(['msg' => 'user does not exist'], 404);
        }

        // verifying hased user password is matching with input password or not
        $authenticatePassword = password_verify($inputPassword, $user->password);

        // password matched
        if ($authenticatePassword) {
            return response()->json(['msg' => 'success'], 200);
        }

        // password not matched
        return response()->json(['msg' => 'unauthorized'], 401);
    }

    /**
     * Handle a login request from to the application.
     * @method POST
     * @param  App\Http\Requests\LoginRequest  $request
     * @return view/ValidationException
     */
    public function login(LoginRequest $request)
    {
        $employeeId = $request->input('employeeId');
        $response = $this->authenticate($request);
        if ($response->getStatusCode() === 200) {
            $request->session()->put('loggedInId', $employeeId);
            return Redirect::route('home', [$employeeId]);
        } else {
            throw ValidationException::withMessages([
                'password' => [trans('auth.failed')],
            ]);
        }
    }

    /**
     * Handle a logout request from to the application.
     * @method POST
     * @return view
     */
    public function logout()
    {
        request()->session()->forget('loggedInId');
        return Redirect::route('login');
    }

    /**
     * Get update password page
     * @method GET
     * @return view
     */
    public function getUpdatePasswordPage()
    {
        return view('auth.update-password', ['forceUpdatePassword' => true]);
    }

    /**
     * Update password
     * @method POST
     * @guard web
     * @param Request $request
     */
    public function updatePassword(Request $request)
    {
        $inputEmployeeId = $request->input('employeeId');
        $inputEmployeeId = strtolower($inputEmployeeId);
        $inputOldPassword = $request->input('oldPassword');
        $inputNewPassword = $request->input('oldPassword');

        $request->validate([
            'employeeId' => 'required|bail|string|min:2|max:255',
            'oldPassword' => 'required|bail|string|min:8',
            'password' => 'required|bail|string|min:8|confirmed'
        ]);

        // retrieving the user
        $user = User::where('employee_id', $inputEmployeeId)->first();

        // user does not exist
        if (!$user) {
            throw ValidationException::withMessages([
                'employeeId' => ['User does not exist with mentioned employee id'],
            ]);
        }

        // verifying hased user password is matching with input old password or not
        $authenticatePassword = password_verify($inputOldPassword, $user->password);

        // password matched
        if ($authenticatePassword) {
            // let user update it's password
            $user->update([
                'password' => password_hash($inputNewPassword, PASSWORD_DEFAULT),
                'password_updated_at' => now()
            ]);
            return Redirect::route('home', [$inputEmployeeId]);
        }

        // password not matched
        throw ValidationException::withMessages([
            'oldPassword' => [trans('auth.failed')],
        ]);
    }
}
