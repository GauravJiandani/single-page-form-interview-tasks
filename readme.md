Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web project.

## About Interview Tasks

#  1) Create an Authentication API system in Laravel that lets you authenticate yourself by sending a JSON payload with your employee id and password.



* git clone https://GauravJiandani@bitbucket.org/GauravJiandani/single-page-form-interview-tasks.git

* composer install

* php artisan key:generate

* Set DB configuration

* php artisan migrate --seed



Seeded Values:-

> 'name' => 'Gaurav', 'employeeId' => '1gaurav', 'password' => '1Gaurav',

> 'name' => 'Tejas', 'employeeId' => '2tejas', 'password' => '1Tejas',

> 'name' => 'Abhinav', 'employeeId' => '2abhinav', 'password' => '3Abhinav'



* Test 1) Hit POST api route: http://127.0.0.1:8000/api/authenticate with json payload as { "employeeId": "1gaurav", "password": "1Gaurav"}, ASSERT response as {"msg": "success"}

* Test 2) Hit POST api route: http://127.0.0.1:8000/api/authenticate with json payload as { "employeeId": "1gaurav", "password": "1Gauravvvvvvvvvvvv"}, ASSERT response as {"msg": "unauthorized"}

-----
Execution description:

-> Added abstracted request class APIFormRequest which extends FormRequest class and thus responsible for throwing validation exceptions

-> Added TransformInputToLowerCase.php which is responsible for string lower casing

-> AuthController with authenticate method and RegisterController with register method performing aforementioned functionalities

-> UsersTableSeeder seeding initial values for testing

-> login and register view

-> added CORS middleware and enabled in Kernel.php

-> added partials for employeeId and password elements

---
---
---

## RolePriority

RoleController has following methods:-
1) Assign Role - assignRole(employeeId, roleSlug) - POST: http://127.0.0.1:8000/api/user/{employeeId}/assign/role/{roleSlug}/
2) Expel Role - expelRole(employeeId, roleSlug) - POST: http://127.0.0.1:8000/api/user/{employeeId}/assign/role/{roleSlug}/
3) Find highest priority Role = findHighestPriorityRole(employeeId)
GET: http://127.0.0.1:8000/api/role/highest-priority/1Gaurav

Seeded Roles:-


|Employee ID                |Roles                                         |
|----------------|-------------------------------|-----------------------------|
|1gaurav|`'Inventory executive, Co-Admin'`            |            |
|2tejas          |`'Admin'`            |            |
|3abhinav          |`Co-Admin`||

---------

|Roles (Slug)                |Priority (1: high, 3: low)                                         |
|----------------|-------------------------------|-----------------------------|
|Admin (admin)|`1`            |            |
|Co-Admin (co_admin)          |`2`            |
|Sales Manager (sales_manager)          |`2`
|Inventory Executive (inventory_exec)          |`3`
|Sales Representative (sale_rep)          |`3`||


-------
-------

## **Implement a password change reminder system in Laravel that reminds users to change their passwords every month.**

Flow chart for WEB APPLICATION:

> php artisan tinker
>
> $user=User::find(1); (Gaurav User) (EmployeeId: 1Gaurav, Password 1Gaurav)
>
> Set user's last password change (password_updated_at:` 2019-09-21 21:09:22`
> $user->update(['password_updated_at'=>'2019-09-21 21:09:22']);
>
> After serving `php artisan serve`
>
> Open `127.0.0.1:8000` in browser
>
> Login as user (EmployeeId: 1Gaurav, Password 1Gaurav)
>
> After successfull login, it will take you to Password update page, if the password wouldn't have been exceeded for a month then it would have taken straight away to HOME PAGE

----

> A[User] -- Login functionality --> B((Login Page))
>
> B((Login Page)) --> C(Password Perished Middleware)
>
> C(Password Perished Middleware) --> D{Home Page}
>
> D{Home Page} --> E((Logout))
>
> E((Logout)) --> B(Login Page)

----

### Our application is live and we now realize that we have to rename the “role” column in our “users” table to “role_id”, this also affects our REST API that a lot of our customers are using. How would you go about changing the column without causing any downtime?

There are 2 approaches for this:-

* Create a Migration that will rename the column. Secondly, in same migration CREATE a VIEW which will be useful for READ-ONLY purposes with same column `role as role_id` so that apis don't get affected, just update the earlier retrieval model with VIEW model.
* Create a Migration that would rename the column and add extra attribute while retrieving records for same.
User::where('id', '>', 0)->get('role_id AS role', 'users.*'])
