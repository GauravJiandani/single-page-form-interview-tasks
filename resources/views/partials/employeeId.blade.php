<div class="form-group row">
    <label for="employeeId" class="col-md-4 col-form-label text-md-right">{{ __('Employee Id') }}</label>

    <div class="col-md-6">
        <input id="employeeId" type="text" class="form-control @error('employeeId') is-invalid @enderror" name="employeeId" value="{{ old('employeeId') }}" required>

        @error('employeeId')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
