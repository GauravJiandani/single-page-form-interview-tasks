<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Authentication Routes...
Route::get('login', 'AuthController@showLoginForm')->name('login');
Route::post('login', 'AuthController@login')->name('login');
Route::get('logout', 'AuthController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'RegisterController@register');

// Password Routes...
Route::get('password/update', 'AuthController@getUpdatePasswordPage')->name('update-password');
Route::post('password/update', 'AuthController@updatePassword');

// Home Routes...
Route::get('home/{employeeId}', 'AuthController@showHomePage')->name('home')->middleware('perished_password');
