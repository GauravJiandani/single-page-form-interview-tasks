<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Authentication Route...
Route::post('authenticate', 'AuthController@authenticate');

// Registration Route...
Route::post('user/register', 'RegisterController@register');

// Role Route...
Route::get('role/highest-priority/{employeeId}', 'RoleController@findHighestPriorityRole');
Route::post('user/{employeeId}/assign/role/{roleSlug}', 'RoleController@assignRole');
Route::post('user/{employeeId}/expel/role/{roleSlug}', 'RoleController@expelRole');
